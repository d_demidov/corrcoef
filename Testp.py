from corrcoef import *
import numpy as np
from math import *

s = (22*888,2)
s3 = np.random.normal(0, 1, s)
s4 = np.random.normal(0, 1, s)

g = 22
fs3 = np.zeros((s[0]//g, s[1]))
fs4 = np.zeros((s[0]//g, s[1]))

for i in range(len(fs3)):
    for j in range(len(fs3[0])):
        fs3[i][j] = s3[i*g][j];
        fs4[i][j] = s4[i*g][j];

C = np.zeros((4,3))
corrcoef(s3, s4, C, 22)

for i in range(len(C[0])):
    p_cor = np.corrcoef(
           np.concatenate((np.transpose(fs3), np.transpose(fs4)), 0)
    )
    fs4 = np.roll(fs4, -1, axis = 0)
    for j in range(len(C)):
        np.testing.assert_almost_equal(C[j][i], p_cor[j//2][2+j%2])
