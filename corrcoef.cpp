#include <boost/multi_array.hpp>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>

#include <vexcl/vexcl.hpp>

#include "cor.hpp"

namespace py = pybind11;

//---------------------------------------------------------------------------
inline void precondition(bool cond, std::string error_message) {
    if (!cond) throw std::runtime_error(error_message);
}

//---------------------------------------------------------------------------
template <size_t N, typename T>
boost::multi_array_ref<T, N> py2boost(py::array_t<T> p) {
    py::buffer_info i = p.request();
    if (i.ndim != N)
        throw std::logic_error(std::string("Incompatible dimensions in py2boost() ") + std::to_string(N) + std::to_string(i.ndim));

    size_t ord[N];
    bool   asc[N];
    for(int j = 0; j < N; ++j) {
        ord[j] = j;
        asc[j] = true;
    }

    std::sort(ord, ord + N, [&i](size_t a, size_t b){
            return i.strides[a] < i.strides[b]; });

    return boost::multi_array_ref<T, N>(static_cast<T*>(i.ptr),
            i.shape, boost::general_storage_order<N>(ord, asc));
}

void corrcoef(
        py::array_t<double> S1,
        py::array_t<double> S2,
        py::array_t<double> C,
        int step
        )
{
    auto s1 = py2boost<2>(S1);
    auto s2 = py2boost<2>(S2);
    auto c  = py2boost<2>(C);

    precondition(s1.shape()[0] == s2.shape()[0] && s1.shape()[1] == s2.shape()[1],
            "Inconsistent S1 and S2 sizes");
    precondition(s1.shape()[1] == 2 && s2.shape()[1] == 2,
            "S1 and S2 should be Nx2 matrices");
    precondition(c.shape()[0] == 4,
            "C should be 4xNS matrix");
    precondition(s1.shape()[0] % step == 0,
            "step doesn't divide length of field signal");

    int ns = c.shape()[1];
    int lfld = s1.shape()[0];

    vex::vector<double> cor_d(ctx(), 4*ns);
    vex::vector<double> s1_d(ctx(), lfld*2, s1.data());
    vex::vector<double> s2_d(ctx(), lfld*2, s2.data());
    fast_cor(cor_d, s1_d, s2_d, ns, step, lfld);

    vex::copy(cor_d, c.data());
}

PYBIND11_PLUGIN(corrcoef) {
    py::module m("corrcoef", "corrcoef");

    m.def("corrcoef", corrcoef, py::arg("S1"), py::arg("S2"), py::arg("C"),
            py::arg("step") = 2);

    return m.ptr();
}
