#include <mutex>

#include <boost/progress.hpp>

#include <vexcl/vexcl.hpp>

const vex::Context& ctx() {
    static vex::Context context( vex::Filter::Exclusive(
                vex::Filter::Env             &&
                vex::Filter::DoublePrecision &&
                vex::Filter::Count(1)
                ) );
    return context;
}

void fast_cor(vex::vector<double>& out,// output, 4*ns size
              vex::vector<double>& S1, // value placed like: x, y, x, y, x
              vex::vector<double>& S2, // value placed like: x, y, x, y, x
              int ns,
              int step,
              int lfld 
              )
{
    std::cout << "lfld = " << lfld << std::endl;
    std::cout << "step = " << step << std::endl;
    std::cout << "ns = " << ns << std::endl;

    vex::vector<double> E(ctx(), 8);
    VEX_FUNCTION(double, expected_values, (int, idx)(int, lfld)(int, step)(double*, S1)(double*, S2)(double*, out_vector), 
                double m = 0; 
                double s = 0; 
                
                if (idx < 2){
                    for(int it = 0, i = idx; it < ((lfld-1)/step+1); ++it, i+=2*step){
                        m += *(S1 + i);
                        s += *(S1 + i) * *(S1 + i);
                    }
                }else{
                    for(int it = 0, i = idx - 2; it < ((lfld-1)/step+1); ++it, i+=2*step){
                        m += *(S2 + i);
                        s += *(S2 + i) * *(S2 + i);
                    }
                }

                m /= ((lfld-1)/step+1);
                *(out_vector + idx) = sqrt(s/((lfld-1)/step+1) - m*m);
                
                return m;
                );

    vex::permutation(vex::element_index(0,4))(E) = expected_values(vex::element_index(0, 4), lfld,step, vex::raw_pointer(S1), vex::raw_pointer(S2), vex::raw_pointer(E) + 4);

    VEX_FUNCTION(double, cor,
                 (int, idx)(int, ns)(int, lfld)(int, step)
                         (double*, S1)(double*, S2)(double*, E),

            int i = idx / ns;
            int j = idx % ns;

            int shift2 = 2*j*step;

            double e1 = E[i/2];
            double e2 = E[2+i%2];
            double std1 = E[4+i/2];
            double std2 = E[4+2+i%2];
            
            double sum = 0;
            for (int it = 0, k1 = i/2, k2 = i%2; it < (lfld-1)/step+1; ++it, k1+=2*step, k2+=2*step)
                sum += (S1[k1] - e1) * (S2[(k2 + shift2) % (2*lfld)] - e2);

            sum /= (lfld-1)/step+1;

            return sum/(std1*std2);
    );

    out = cor(vex::element_index(0,4*ns), ns, lfld, step, vex::raw_pointer(S1), vex::raw_pointer(S2), vex::raw_pointer(E));

}
